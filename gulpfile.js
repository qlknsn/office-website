var gulp = require("gulp");
var less = require("gulp-less");
var browsersync = require("browser-sync").create();
browsersync.init({
  server: { baseDir: ".", index: "src/html/homepage/homepage.html" },
  // server: { baseDir: ".", index: "src/html/support/support.html" },
});

function compileLess(cb) {
  browsersync.reload();
  gulp
    .src("src/**/*.less")
    .pipe(less())
    .pipe(
      gulp.dest(function (f) {
        return f.base;
      })
    );
  cb();
}
function compileHtml(cb) {
  browsersync.reload();
  cb();
}
gulp.watch("src/**/*.less", compileLess);
gulp.watch("src/**/*", compileHtml);
exports.default = compileLess;
