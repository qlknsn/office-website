from fabric.api import env, run,  local

env.user = 'frontend'
env.hosts = ['192.168.199.169']


def test():
    local('git pull origin master')
    local('rm -rf office-website')
    local('mkdir -p office-website && cp -r src office-website  && cp -r assets office-website && cp -r lib office-website')
    local("tar cvzf office-website.tar.gz office-website")
    run('cd /home/frontend/apps/html &&  rm -rf office-website.tar.gz && rm -rf office-website')
    local("scp office-website.tar.gz frontend@192.168.199.169:/home/frontend/apps/html")
    run('cd /home/frontend/apps/html && tar xvzf office-website.tar.gz')

def production():
    local('git checkout production&&git pull origin production')
    local('rm -rf office-website')
    local('mkdir -p office-website && cp -r src office-website  && cp -r assets office-website && cp -r lib office-website')
    local("tar czf office-website.tar.gz office-website")