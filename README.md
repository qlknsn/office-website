## 公司官网

### 开始开发

- 安装必要依赖
  `npm install`
- 运行项目
  `gulp`会自动打开网站首页

### 简短说明

项目在运行`gulp`命令后，只要修改`src`文件夹中的任何文件，浏览器都会自动更新，less 文件也会自动编译成 css 文件。

样式文件统一写在`src/style`目录下，统一编写`*.less`文件，脚本会自动编译成`css`文件，在 html 中引入的样式文件统一引入编译好的`css`文件。

### 正式环境 部署
master分支是测试环境、
production分支是正式环境、
#### 前端部署
本地编译：npm安装所有package.json文件中对应的代码包。
环境依赖：无环境依赖，直接全量静态文件部署，新建空文件夹 office-website,然后把 lib,assets,src 文件夹下的文件统一拷贝到 office-website 目录下即可，然后参照下面 Nginx 配置。

前端代码 Nginx 配置参考代码目录中`frontend-nginx.conf`文件。

### 注意事项

❗ 根目录下的.vscode 文件夹不要删除。
