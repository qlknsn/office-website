(function () {
  var header = document.getElementById("header");
  header.innerHTML =
    '<div class="header-container-inner">' +
    ' <div class="logo-container">' +
    '          <img src="https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/logo.png" alt="" />  ' +
    '</div>' +
    ' <ul class="menu-container">' +
    '<li class="menu-item" onclick="menuItemClick(1)">' +
    '<a>首页</a>' +
    '<span class="spans '+
      (localStorage.getItem("selectedIndex") == 1 ? "spanIsclick" : "")+
    '"></span>' +
    '</li>' +
    '<li class="menu-item" onclick="menuItemClick(2)"><a class="IsHovereds">产品中心</a><span class="spans '+
      (localStorage.getItem("selectedIndex") == 2 ? "spanIsclick" : "")+
    '"></span></li>' +
    '<li class="menu-item" onclick="menuItemClick(3)"><a>案例中心</a><span class="spans '+
      (localStorage.getItem("selectedIndex") == 3 ? "spanIsclick" : "")+
    '"></span></li>' +
    '<li class="menu-item" onclick="menuItemClick(4)"><a class="IsHovereds">解决方案</a><span class="spans '+
      (localStorage.getItem("selectedIndex") == 4 ? "spanIsclick" : "")+
    '"></span></li>' +
    '<li class="menu-item" onclick="menuItemClick(5)"><a>服务与支持</a><span class="spans '+
      (localStorage.getItem("selectedIndex") == 5 ? "spanIsclick" : "")+
    '"></span></li> ' +
    '<li class="menu-item" onclick="menuItemClick(6)"><a class="IsHovereds">关于我们</a><span class="spans '+
      (localStorage.getItem("selectedIndex") == 6 ? "spanIsclick" : "")+
    '"></span></li> ' +
    ' </ul>' +
    '</div>' +
    '<div class="slide-container IsHovereds"></div>' +
    '</header>';
})();

var hoverName = "";
$(".menu-item").hover(
  function () {
    $(".spans", this).stop().css("height", "3px");
    $(".spans", this).animate(
      {
        left: "17%",
        width: "66%",
        right: "0",
      },
      300
    );
    // 点击生成下划线

    // 悬浮显示宽屏菜单
    // console.log($("a", this));
    if($("a", this)[0]){
      hoverName = $("a", this)[0].innerHTML;
    }
    
    if(hoverName){
      switch (hoverName) {
        case "产品中心":
          $(".slide-container")[0].innerHTML = 
          '<div class="chanpin" >'+
            '<div class="chanpinItem">'+
              '<img src="../../../assets/images/chanpin-1@2x.png" alt="">'+
            '</div>'+
            '<div class="chanpinItem">'+
              '<h3>产品中心</h3>'+
              '<p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>'+
              '<p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>'+
            '</div>'+
            '<div class="chanpinItem">'+
              '<p class="chanpinName"><a>智慧安防</a></p>'+
              '<p class="chanpinName"><a>智慧政府</a></p>'+
              '<p class="chanpinName"><a>智慧城市</a></p>'+
              '<p class="chanpinName"><a>行业应用</a></p>'+
              '<p class="chanpinName"><a>智能设备</a></p>'+
          '</div>'+
          '</div>'
          // `        break;
        case "解决方案":
          $(".slide-container")[0].innerHTML = 
          '<div class="chanpin" >'+
            '<div class="chanpinItem">'+
              '<img src="../../../assets/images/chanpin-1@2x.png" alt="">'+
            '</div>'+
            '<div class="chanpinItem">'+
              '<h3>产品中心</h3>'+
              '<p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>'+
              '<p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>'+
            '</div>'+
            '<div class="chanpinItem">'+
              '<p class="chanpinName"><a>智慧安防</a></p>'+
              '<p class="chanpinName"><a>智慧政府</a></p>'+
              '<p class="chanpinName"><a>智慧城市</a></p>'+
              '<p class="chanpinName"><a>行业应用</a></p>'+
              '<p class="chanpinName"><a>智能设备</a></p>'+
          '</div>'+
          '</div>'
          break;
        case "关于我们":
          $(".slide-container")[0].innerHTML = 
          '<div class="chanpin" >'+
            '<div class="chanpinItem">'+
              '<img src="../../../assets/images/chanpin-1@2x.png" alt="">'+
            '</div>'+
            '<div class="chanpinItem">'+
              '<h3>关于我们</h3>'+
              '<p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>'+
              '<p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>'+
            '</div>'+
            '<div class="chanpinItem">'+
              '<p class="chanpinName"><a>公司简介</a></p>'+
              '<p class="chanpinName"><a>新闻资讯</a></p>'+
              '<p class="chanpinName"><a>联系我们</a></p>'+
          '</div>'+
          '</div>'
          // `
          // <div class="chanpin" >
          //   <div class="chanpinItem">
          //     <img src="../../../assets/images/chanpin-1@2x.png" alt="">
          //   </div>
          //   <div class="chanpinItem">
          //     <h3>关于我们</h3>
          //     <p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>
          //     <p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>
          //   </div>
          //   <div class="chanpinItem">
          //     <p class='chanpinName'><a>公司简介</a></p>
          //     <p class='chanpinName'><a>新闻资讯</a></p>
          //     <p class='chanpinName'><a>联系我们</a></p>
          //   </div>
          // </div>
          // `;
          break;
      }
    }
    

    if ($("a", this)["0"]&&$("a", this)["0"].className.includes("IsHovereds")) {
      $(".slide-container").stop().slideDown(200);
    } else {
      $(".slide-container").stop().slideUp(200);
    }
  },
  function () {
    // 全部隐藏

    // $('a').removeClass('IsHovereds')
    // console.log($('span'))//查看当前选中的项
    if ($(".spans", this)["0"]&&$(".spans", this)["0"].className.includes("spanIsclick")) {
    } else {
      $(".spans", this).stop().animate(
        {
          left: "50%",
          width: "0",
        },
        300
      );
    }
    if ($(".slide-container")["0"].className.includes("IsHovereds")) {
      $(".spans").css({ height: "0", width: "0", left: "50%" });
      $(".spans").each(function () {
        if ($(this)[0].className.includes("spanIsclick")) {
          $(this).css({ height: "3px", width: "66%", left: "17%" });
        }
      });
    } else {
      // 收起宽屏
      $(".slide-container").stop(true, true).slideUp(200);
    }
  }
);
$(".slide-container").hover(
  function () {},
  function () {
    $(".slide-container").stop(true, true).slideUp(200);
  }
);

// window.onload = function () {
//   localStorage.setItem("selectedIndex", 1);
//   // localStorage
// };

let menuItemClick = (data) => {
  // console.log($(".spans")[data - 1]);
  localStorage.setItem("selectedIndex", data);
  // href="/"
  // href="/src/html/production/production.html"
  // href="/src/html/casecenter/caseCenter.html"
  // href="/src/html/resolution/resolution.html"
  // href="/src/html/support/support.html"
  // href="/src/html/aboutus/about.html"
  $(".spans").removeClass("spanIsclick");
  $(".spans").css({ height: "0", width: "0", left: "50%" });
  $(".spans:eq("+(data - 1)+")").addClass("spanIsclick");
  $(".spans:eq("+(data - 1)+")").css({ height: "3px", width: "66%", left: "17%" });
  switch (data) {
    case 1:
      window.location.href = "/";
      break;
    case 2:
      window.location.href = "/src/html/production/production.html";
      break;
    case 3:
      window.location.href = "/src/html/casecenter/caseCenter.html";
      break;
    case 4:
      window.location.href = "/src/html/resolution/resolution.html";
      break;
    case 5:
      window.location.href = "/src/html/support/support.html";
      break;
    case 6:
      window.location.href = "/src/html/aboutus/about.html";
      break;
  }
};
