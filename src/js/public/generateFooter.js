(function () {
  
  var header = document.getElementById("footer");
  header.innerHTML =
    '<div class="homepage-footer" id="footercontent">' +
    '<div class="homepage-footer-con">' +
    '<div class="homepage-footer-con-item">' +
    '<img src="https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/logo-white.png" alt="">' +
    '<div class="homepage-footer-con-item-img">' +
    '<img src="https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/erweima.jpg" alt="">' +
    '<br>' +
    '猎熊座&nbsp;&nbsp;公众号' +
    '</div>' +
    '</div>' +
    '<div class="homepage-footer-con-item">' +
    '<div class="homepage-footer-con-item-text">' +
    '<span class="footerTitle">关于我们</span>' +
    '<br>' +

    '公司简介' +
    '<br>' +
    '新闻资讯' +
    '<br>' +
    ' 联系我们' +
    '</div>' +
    '<div class="homepage-footer-con-item-text">' +
    '<span class="footerTitle">全国客服电话</span>' +
    '<br>' +
    '<span>AM9:00-PM18:00</span>' +
    '<br>' +
    '<span class="homepage-footer-con-item-phone">021-68018888</span>' +
    '</div>' +

    '</div>' +
    '<div class="homepage-footer-con-item">' +
    '猎熊座安全技术(上海)有限公司 © 公司电话:021-68018888 hunter@bearhunting.cn' +
    '<br>' +
    '上海市浦东新区沈梅路123弄 星月智汇湾15号楼<a style="display:inline;text-decoration:none;color:white;" href="https://beian.miit.gov.cn">沪ICP备15039279号-1</a>'+
    '</div>' +
    '</div>' +
    '</div>'
  $('#footercontent').css('opacity', 0)
  $(window).scroll(function () {
    var scrollTop = $(this).scrollTop();
    var scrollHeight = $(document).height();
    var windowHeight = $(this).height();
    // console.log(scrollTop,windowHeight,scrollHeight)
    if (scrollHeight-(scrollTop + windowHeight) < 2 ) {
      $('#footercontent').css('opacity', 1)
    } else {
      $('#footercontent').css('opacity', 0)
    }
    
  });
  if($(document).height()-$(window).height()<3){
    $('#footercontent').css('opacity', 1)
  }
 
})();
