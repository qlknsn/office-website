
// console.log(ARTICLE_URL)
// 走马灯
var mySwiper = new Swiper(".homepage-carousel", {
  direction: "horizontal", // 垂直切换选项
  loop: true, // 循环模式选项
  autoplay: {
    delay: 10000,
  },
  speed: 1000,
  // // 如果需要分页器
  pagination: {
    el: ".swiper-pagination",
  },


  // 如果需要前进后退按钮
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

// 公司产品/案例
var productionUrl = [
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/anfang-1.png",
    selected: true,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/anfang.png",
    name: "智慧安防",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/weikako@2x.png",
        search: "hangye",
        title: "微卡口",
        text:
          "基于地理信息系统的摄像资源结构化及大数据分析，实时显示接入系统平台的的所有摄像资源地理分布及相关信息",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/fangfan@2x.png",
        search: "hangye",
        title: "综合智能防范服务",
        text:
          "智能防范物联网服务平台”是公安机关用于社会治理、治安防范、民生服务等领域的综合服务平台。",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/WIFI@2x.png",
        search: "hangye",
        title: "WIFI嗅探",
        text:
          "公安部需要采取更加精细化的管理和监控，为了便加强对于现代化城市的管理，打击违法犯罪行为",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zhengwu-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zhengwu.png",
    name: "智慧政务",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/cunju@2x.png",
        search: "zhengwu",
        title: "村居联勤联动微平台",
        text:
          "作为社会治理赋能平台，街镇依托城运分中心和联勤联动站（村居平台），以“三共享双推送”等方式实现五大功能。",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zongzhi@2x.png",
        search: "zhengwu",
        title: "综治智慧平台",
        text:
          "致力于构建“一体指挥、多方联动、常态运作”的城市综合治理中心，初步实现了政府牵头、多方参与、齐抓共管的联动联管联防机制",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renyuanku@2x.png",
        search: "zhengwu",
        title: "特殊人员数据库",
        text:
          "台的建设目标是提供信息化手段，简化工作人员业务流程，便于部门内信息共享，以及历史记录的追溯",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chengshi-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chengshi.png",
    name: "智慧城市",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chengshi@2x.png",
        search: "chengshi",
        title: "城市运行综合管理平台",
        text:
          "将原有监控视频进行信息化、智慧化融合，充分运用物联网、大数据、云计算等技术，通过对“人、车、物”三方面的管理...",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/yingyong-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/yingyong.png",
    name: "行业应用",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/geli@2x.png",
        search: "hangye",
        title: "猎熊座防疫隔离智能检测系统",
        text:
          "通过物联网、大数据手段，实现了“被隔离人员进出隔离场所”的实时预警功能。系统由前端监测采集器和后台数据分析处理系统组成。",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zhatuche@2x.png",
        search: "hangye",
        title: "渣土车城市运行综合管理智慧平台",
        text:
          "利用AI渣土车识别和RF-LOT智能感知技术，自动抓取渣土车辆高清图片，自动识别车辆车牌、车型等信息",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/kozhao@2x.png",
        search: "hangye",
        title: "猎熊座口罩检测智能监测系统",
        text:
          "本产品主要应用于公共区域的人员口罩佩戴的智能检测，对实时捕捉到的画面分析，如有存在安全隐患的行为，实时进行语音提醒警报",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/shebei-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/shebei.png",
    name: "智能设备",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/GF010@2x.png",
        search: "shebei",
        title: "智能设备",
        text:
          "猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/shouhuan@2x.png",
        search: "shebei",
        title: "老人手环",
        text:
          "老人智能手环运用公安物联网、RF-LOT智能感知等技术，可实时监测老人的行动轨迹，一旦发生老人走失，及时追踪寻回走失老人。",
      },
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/fangyi@2x.png",
        search: "shebei",
        title: "防疫隔离智能监测系统",
        text:
          "采用4G无线监控球型摄像机用于疫情人员隔离观察及外出监管的防控系统，以便于基层防控人员对需要进行隔离观察的人群进行有效的管控。",
      },
    ],
  },
];
var selectedInner = [];

var productionInner = () => {
  var str = "";
  productionUrl.forEach((item, index) => {
    // console.log(item.childern)
    str += "<div class='pro-list-item"+(item.selected ? ' pro-list-item-selected' : ' ')+"' onclick='prolistitemClick(this,"+index+")'>"+
    "<img src='"+(item.selected ? item.sel : item.unsel)+"' alt=''></br>"+
    item.name+
    "</div>"

    // `
    //   <div class="pro-list-item ${item.selected ? "pro-list-item-selected" : ""}" onclick='prolistitemClick(this,${index})'>
    //     <img src="${item.selected ? item.sel : item.unsel}" alt="">
    //
    //     ${item.name}
    //   </div>
    // `;
    return str;
  });
  // console.log($(".pro-list"))
  $(".pro-list")[0].innerHTML = str;

  let strPro = '';
  productionUrl[0].childern.forEach((item, index) => {
    strPro +=
      '<div class="list-detail-con-item proclick list-detail-con-item-'+(index + 1)+' '+item.search+'">'+
      '<div class="modelImg">'+
      '<img src="'+item.image+'"></img>'+
      '</div>'+
      '<div class="modelTitle">'+
      item.title+
      '</div>'+
      '<div class="modelText">'+
      item.text+
      '</div>'+
      '<div class="modelButton">'+
      '<button>了解更多</button>'+
      '</div>'+
      '</div>'
  });

  $(".list-detail-con")[0].innerHTML = strPro;

  setTimeout(() => {
    $(".list-detail-con-item-1").animate({ marginTop: "0px" }, 300);
    $(".list-detail-con-item-2").animate({ marginTop: "0px" }, 400);
    $(".list-detail-con-item-3").animate({ marginTop: "0px" }, 500);
  }, 100);
};

let anliUrl = [
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renlian-1.png",
    selected: true,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renlian.png",
    name: "人脸识别",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/case1.png",
        title: "人脸识别",
        text:
          "猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chepai-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chepai.png",
    name: "车牌识别",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/case2.png",
        title: "车牌识别",
        text:
          "猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/diandongche-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/diandongche.png",
    name: "电动车防盗系统",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/case4.png",
        title: "电动车防盗系统",
        text:
          "猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renqun-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renqun.png",
    name: "人群密度分析",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/case7.png",
        title: "人群密度分析",
        text:
          "猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。",
      },
    ],
  },
  {
    unsel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/daozha-1.png",
    selected: false,
    sel: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/daozha.png",
    name: "小区道闸管理",
    childern: [
      {
        image: "https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/case5.png",
        title: "小区道闸管理",
        text:
          "猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。",
      },
    ],
  },
];
let anliInner = () => {
  let str = "";
  anliUrl.forEach((item, index) => {
    str += "<div class='pro-list-item"+(item.selected ? ' pro-list-item-selected' : ' ')+"' onclick='anliListitemClick(this,"+index+")'>"+
    "<img src='"+(item.selected ? item.sel : item.unsel)+"' alt=''></br>"+
    item.name+
    "</div>"
    return str;
  });
  $(".pro-list")[0].innerHTML = str;

  let strAnli = '';

  anliUrl[0].childern.forEach((item, index) => {
    strAnli +=
    '<div class="list-detail-con-item proclick list-detail-con-item-'+(index + 1)+'">'+
    '<div class="modelImg">'+
    '<img src="'+item.image+'"></img>'+
    '</div>'+
    '<div class="modelTitle">'+
    item.title+
    '</div>'+
    '<div class="modelText">'+
    item.text+
    '</div>'+
    '<div class="modelButton">'+
    '<button>了解更多</button>'+
    '</div>'+
    '</div>'
  });

  $(".list-detail-con")[0].innerHTML = strAnli;

  setTimeout(() => {
    $(".list-detail-con-item-1").animate({ marginTop: "0px" }, 300);
    $(".list-detail-con-item-2").animate({ marginTop: "0px" }, 400);
    $(".list-detail-con-item-3").animate({ marginTop: "0px" }, 500);
  }, 100);
};
productionInner();
$(".pro-title").click(function () {
  $(".pro-span").removeClass("pro-selected");
  $(".pro-span", this).addClass("pro-selected");
  if ($(".pro-title", this)["prevObject"][0].innerText == "案例中心") {
    anliInner();
    anliselect = 0;
    proselect = "";
  } else {
    productionInner();
    anliselect = "";
    proselect = 0;
  }
});

let proselect = 0;
let anliselect = "";
let prolistitemClick = (data, index) => {
  productionUrl.forEach((it, i) => {
    it.selected = false;
    if (i == index) {
      it.selected = true;
    }
  });
  proselect = index;
  productionInner();
  let str = '';

  productionUrl[index].childern.forEach((item, index) => {
    str +=
    '<div class="list-detail-con-item proclick list-detail-con-item-'+(index + 1)+' '+item.search+'">'+
      '<div class="modelImg">'+
      '<img src="'+item.image+'"></img>'+
      '</div>'+
      '<div class="modelTitle">'+
      item.title+
      '</div>'+
      '<div class="modelText">'+
      item.text+
      '</div>'+
      '<div class="modelButton">'+
      '<button>了解更多</button>'+
      '</div>'+
      '</div>'
    // `
    //     <div class="list-detail-con-item list-detail-con-item-${
    //       index + 1
    //     }" onclick=''>
    //       <div class='modelImg'>
    //         <img src='${item.image}'></img>
    //       </div>
    //       <div class='modelTitle'>
    //         ${item.title}
    //       </div>
    //       <div class='modelText'>
    //         ${item.text}
    //       </div>
    //       <div class='modelButton'>
    //         <button>了解更多</button>
    //       </div>
    //     </div>
    //   `;
  });
  $(".list-detail-con")[0].innerHTML = str;

  listJump();
};
let anliListitemClick = (data, index) => {
  anliUrl.forEach((it, i) => {
    it.selected = false;
    if (i == index) {
      it.selected = true;
    }
  });
  anliselect = index;
  anliInner();
  let str = '';
  // console.log(anliUrl[index].childern);
  anliUrl[index].childern.forEach((item, index) => {
    str +=
    '<div class="list-detail-con-item proclick list-detail-con-item-'+(index + 1)+'">'+
    '<div class="modelImg">'+
    '<img src="'+item.image+'"></img>'+
    '</div>'+
    '<div class="modelTitle">'+
    item.title+
    '</div>'+
    '<div class="modelText">'+
    item.text+
    '</div>'+
    '<div class="modelButton">'+
    '<button>了解更多</button>'+
    '</div>'+
    '</div>'
    // `
    //     <div class="list-detail-con-item list-detail-con-item-${
    //       index + 1
    //     }" onclick=''>
    //       <div class='modelImg'>
    //         <img src='${item.image}'></img>
    //       </div>
    //       <div class='modelTitle'>
    //         ${item.title}
    //       </div>
    //       <div class='modelText'>
    //         ${item.text}
    //       </div>
    //       <div class='modelButton'>
    //         <button class='"${item.search}"'>了解更多</button>
    //       </div>
    //     </div>
    //   `;
  });
  $(".list-detail-con")[0].innerHTML = str;
  listJump();
};

let listJump = (data) => {
  $(".list-detail-con-item-1").css({ marginTop: "450px" });
  $(".list-detail-con-item-2").css({ marginTop: "450px" });
  $(".list-detail-con-item-3").css({ marginTop: "450px" });
  setTimeout(() => {
    $(".list-detail-con-item-1").animate({ marginTop: "0px" }, 300);
    $(".list-detail-con-item-2").animate({ marginTop: "0px" }, 400);
    $(".list-detail-con-item-3").animate({ marginTop: "0px" }, 500);
  }, 100);
};
listJump();

$(document).on("mouseenter", ".list-detail-con-item", function () {
  $(this).css({
    "box-shadow": "5px 5px 10px #ddd",
    transition: "all .5s",
    "margin-top": "-10px",
  });
});

$(document).on("click", ".list-detail-con-item", function () {
  // console.log(proselect);
  // console.log(anliselect);
  switch (proselect) {
    case 0:
      localStorage.setItem("selectedIndex", 2);
      window.location.href = "src/html/production/production.html?anfang";
      break;
    case 1:
      localStorage.setItem("selectedIndex", 2);
      window.location.href = "src/html/production/production.html?zhengwu";
      break;
    case 2:
      localStorage.setItem("selectedIndex", 2);
      window.location.href = "src/html/production/production.html?chengshi";
      break;
    case 3:
      localStorage.setItem("selectedIndex", 2);
      window.location.href = "src/html/production/production.html?hangye";
      break;
    case 4:
      localStorage.setItem("selectedIndex", 2);
      window.location.href = "src/html/production/production.html?shebei";
      break;
    default:
      break;
  }
  switch (anliselect) {
    case 0:
      localStorage.setItem("selectedIndex", 3);
      window.location.href = "src/html/casecenter/caseCenter.html#renlian";
      break;
    case 1:
      localStorage.setItem("selectedIndex", 3);
      window.location.href = "src/html/casecenter/caseCenter.html#chepai";
      break;
    case 2:
      localStorage.setItem("selectedIndex", 3);
      window.location.href = "src/html/casecenter/caseCenter.html#diandongche";
      break;
    case 3:
      localStorage.setItem("selectedIndex", 3);
      window.location.href = "src/html/casecenter/caseCenter.html#renqun";
      break;
    case 4:
      localStorage.setItem("selectedIndex", 3);
      window.location.href = "src/html/casecenter/caseCenter.html#daozha";
      break;

    default:
      break;
  }
});
$(document).on("mouseleave", ".list-detail-con-item", function () {
  $(this).css({
    "box-shadow": "none",
    transition: "none",
    "margin-top": "0px",
  });
});

let getallTags = (data) => {
  if (data.length > 3) {
    data.splice(0, 3);
  }

  let str = "";
  if (data) {
    data.forEach((it) => {
      str += "<span>"+it+"</span>";
    });
  }
  return str;
};
// 请求新闻
var top3 =[] 

let getNews =function() {
  let axioss = axios.create({
    timeout:60000,
  })
  axioss.interceptors.response.use(response => {
    return response.data;
  }, (err=>{
    console.error(err)
    // console.log(1111)
    // console.log(axioss.interceptors.response.handlers[0].rejected.get('Scopes'))
  }));
  axioss
  .get(ARTICLE_URL, {
    params: { limit: 10, current: 1 },
  })
  .then((res) => {
    if (res.code == '1') {
      if(res.data.length>3){
          top3 = res.data.splice(0, 3);
      }else{
        top3 = res.data
      }
      $(".homepage-news-con-box")[0].innerHTML = "";
      top3.forEach((item,index) => {
        
        $(".homepage-news-con-box")[0].innerHTML +=
        '<div class="homepage-news-con-item" onclick="getNewsDetail(this,'+index+')">'+
        '<div class="homepage-news-con-item-top" style="background:url('+item.cover+') no-repeat;background-size: 100% 100%;"></div>'+
        '<div class="homepage-news-con-item-bottom">'+
          '<div class="homepage-news-con-item-bottom-title">'+
            item.title +
          '</div>'+
          '<div class="homepage-news-con-item-bottom-time">'+
            item.createTime+
          '</div>'+
          '<div class="homepage-news-con-item-bottom-tags">' +
          getallTags(item.tags) +
          '</div>'+
        '</div>'+
      '</div>'
      //     `
      // <div class="homepage-news-con-item">
      //   <div class="homepage-news-con-item-top" style="background:url(${item.cover}) no-repeat;background-size: 100% 100%;"></div>
      //   <div class="homepage-news-con-item-bottom">
      //     <div class="homepage-news-con-item-bottom-title">
      //       ${item.title}
      //     </div>
      //     <div class="homepage-news-con-item-bottom-time">
      //       ${item.createTime}
      //     </div>
      //     <div class="homepage-news-con-item-bottom-tags">` +
      //     getallTags(item.tags) +
      //     `</div>
      //   </div>
      // </div>
      // `;
      });
      console.log( $(".homepage-news-con-box")[0])
    }else{
      // console.error('报错了')
      tracking('ErrorCollection',{
        url: T_SERVER_API,
        errorType:'API_ERR',
        message:res.data,
        // router:'',
        ext:'office_website',
      },TOKEN)
    }
  });
} 
let getNewsDetail = function (a,b) {
  let id = top3[b]['_id']
  // console.log(newslist[b]['_id'])
  window.location.href = "/src/html/aboutus/newsDetail.html?"+id;
}
