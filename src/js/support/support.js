// homepage.js
var hoverName = ''
$('.menu-item').hover(function(){
    $('.spans',this).stop().css("height",'3px');
    $('.spans',this).animate({
      left:'17%',
      width:'66%',
      right:'0',
    },300)
    // 点击生成下划线
    $('.menu-item').click(function(){
      $('.spans').removeClass('spanIsclick');
      $('.spans').css({'height':'0','width':'0',left:'50%'})
      $('.spans',this).addClass('spanIsclick');
      $('.spans',this).css({'height':'3px',width:'66%',left:'17%',})
    })
    // 悬浮显示宽屏菜单

    hoverName = $('a',this)[0].innerHTML
    switch (hoverName) {
      case '产品中心':
        $('.slide-container')[0].innerHTML = 
      ' <div class="chanpin" >'+
      '    <div class="chanpinItem">'+
      '      <img src="https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chanpin-1@2x.png" alt="">'+
      '    </div>'+
      '    <div class="chanpinItem">'+
      '      <h3>产品中心</h3>'+
      '      <p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>'+
      '      <p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>'+
      '    </div>'+
      '    <div class="chanpinItem">'+
      '      <p class="chanpinName"><a>智慧安防</a></p>'+
      '      <p class="chanpinName"><a>智慧政府</a></p>'+
      '      <p class="chanpinName"><a>智慧城市</a></p>'+
      '      <p class="chanpinName"><a>行业应用</a></p>'+
      '      <p class="chanpinName"><a>智能设备</a></p>'+
      '    </div>'+
      '   </div>'
        // `
        // <div class="chanpin" >
        //   <div class="chanpinItem">
        //     <img src="../../../assets/images/chanpin-1@2x.png" alt="">
        //   </div>
        //   <div class="chanpinItem">
        //     <h3>产品中心</h3>
        //     <p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>
        //     <p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>
        //   </div>
        //   <div class="chanpinItem">
        //     <p class="chanpinName"><a>智慧安防</a></p>
        //     <p class="chanpinName"><a>智慧政府</a></p>
        //     <p class="chanpinName"><a>智慧城市</a></p>
        //     <p class="chanpinName"><a>行业应用</a></p>
        //     <p class="chanpinName"><a>智能设备</a></p>
        //   </div>
        // </div>
        // `
        break;
      case '解决方案':
        $('.slide-container')[0].innerHTML = 
      ' <div class="chanpin" >'+
      '    <div class="chanpinItem">'+
      '      <img src="https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chanpin-1@2x.png" alt="">'+
      '    </div>'+
      '    <div class="chanpinItem">'+
      '      <h3>解决方案</h3>'+
      '      <p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>'+
      '      <p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>'+
      '    </div>'+
      '    <div class="chanpinItem">'+
      '      <p class="chanpinName"><a>智慧安防</a></p>'+
      '      <p class="chanpinName"><a>智慧政府</a></p>'+
      '      <p class="chanpinName"><a>智慧城市</a></p>'+
      '      <p class="chanpinName"><a>行业应用</a></p>'+
      '      <p class="chanpinName"><a>智能设备</a></p>'+
      '    </div>'+
      '   </div>'
        // `
        // <div class="chanpin" >
        //   <div class="chanpinItem">
        //     <img src="../../../assets/images/chanpin-1@2x.png" alt="">
        //   </div>
        //   <div class="chanpinItem">
        //     <h3>解决方案</h3>
        //     <p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>
        //     <p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>
        //   </div>
        //   <div class="chanpinItem">
        //     <p class='chanpinName'><a>智慧安防</a></p>
        //     <p class='chanpinName'><a>智慧政府</a></p>
        //     <p class='chanpinName'><a>智慧城市</a></p>
        //     <p class='chanpinName'><a>行业应用</a></p>
        //     <p class='chanpinName'><a>智能设备</a></p>
        //   </div>
        // </div>
        // `
        break;
      case '关于我们':
        $('.slide-container')[0].innerHTML = 
      ' <div class="chanpin" >'+
      '    <div class="chanpinItem">'+
      '      <img src="https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chanpin-1@2x.png" alt="">'+
      '    </div>'+
      '    <div class="chanpinItem">'+
      '      <h3>关于我们</h3>'+
      '      <p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>'+
      '      <p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>'+
      '    </div>'+
      '    <div class="chanpinItem">'+
      '      <p class="chanpinName"><a>智慧安防</a></p>'+
      '      <p class="chanpinName"><a>智慧政府</a></p>'+
      '      <p class="chanpinName"><a>智慧城市</a></p>'+
      '      <p class="chanpinName"><a>行业应用</a></p>'+
      '      <p class="chanpinName"><a>智能设备</a></p>'+
      '    </div>'+
      '   </div>'
        // `
        // <div class="chanpin" >
        //   <div class="chanpinItem">
        //     <img src="../../../assets/images/chanpin-1@2x.png" alt="">
        //   </div>
        //   <div class="chanpinItem">
        //     <h3>关于我们</h3>
        //     <p>猎熊座以"连接、赋能、汇聚、应用"为理念，深耕城市公共</p>
        //     <p>安全、智慧社区、公共防灾领域，公司积极转化研发成果</p>
        //   </div>
        //   <div class="chanpinItem">
        //     <p class='chanpinName'><a>公司简介</a></p>
        //     <p class='chanpinName'><a>新闻资讯</a></p>
        //     <p class='chanpinName'><a>联系我们</a></p>
        //   </div>
        // </div>
        // `
        break;
    }

    if($('a',this)['0'].className.includes('IsHovereds')){
        $('.slide-container').stop().slideDown(200);

    }else{
        $('.slide-container').stop().slideUp(200);

    }


  },function(){
    // 全部隐藏


    // $('a').removeClass('IsHovereds')
    // console.log($('span'))//查看当前选中的项
    if($('.spans',this)['0'].className.includes('spanIsclick')){

    }else{
      $('.spans',this).stop().animate({
        left:'50%',
        width:'0'
      },300)
    }
    if($('.slide-container')['0'].className.includes('IsHovereds')){

        $('.spans').css({'height':'0','width':'0',left:'50%'})
        $('.spans').each(function() {
            if($(this)[0].className.includes('spanIsclick')){
                $(this).css({'height':'3px',width:'66%',left:'17%',})
            }
        })

    }else{
        // 收起宽屏
        $('.slide-container').stop(true,true).slideUp(200);
    }
  })
$('.slide-container').hover(function(){

},function() {
  $('.slide-container').stop(true,true).slideUp(200);
})

// 走马灯
var mySwiper = new Swiper ('.homepage-carousel', {
  direction: 'horizontal', // 垂直切换选项
  loop: true, // 循环模式选项
  autoplay : {
    delay: 5000
  },
  speed:1000,
  // // 如果需要分页器
  pagination: {
    el: '.swiper-pagination',
  },

  // 如果需要前进后退按钮
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },

})

// 公司产品/案例
let productionUrl =  [
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/anfang-1.png',selected:true, sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/anfang.png',name:'智慧安防',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/weikako@2x.png',title:'微卡口',text:'基于地理信息系统的摄像资源结构化及大数据分析，实时显示接入系统平台的的所有摄像资源地理分布及相关信息'},
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/fangfan@2x.png',title:'综合智能防范服务',text:'智能防范物联网服务平台”是公安机关用于社会治理、治安防范、民生服务等领域的综合服务平台。'},
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/WIFI@2x.png',title:'WIFI嗅探',text:'公安部需要采取更加精细化的管理和监控，为了便加强对于现代化城市的管理，打击违法犯罪行为'},
  ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zhengwu-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zhengwu.png',name:'智慧政务',childern:[
      {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/cunju@2x.png',title:'村居联勤联动微平台',text:'作为社会治理赋能平台，街镇依托城运分中心和联勤联动站（村居平台），以“三共享双推送”等方式实现五大功能。'},
      {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zongzhi@2x.png',title:'综治智慧平台',text:'致力于构建“一体指挥、多方联动、常态运作”的城市综合治理中心，初步实现了政府牵头、多方参与、齐抓共管的联动联管联防机制'},
      {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renyuanku@2x.png',title:'特殊人员数据库',text:'台的建设目标是提供信息化手段，简化工作人员业务流程，便于部门内信息共享，以及历史记录的追溯'},
  ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chengshi-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chengshi.png',name:'智慧城市',
    childern:[
      {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chengshi@2x.png',title:'城市运行综合管理平台',text:'将原有监控视频进行信息化、智慧化融合，充分运用物联网、大数据、云计算等技术，通过对“人、车、物”三方面的管理...'},
    ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/yingyong-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/yingyong.png',name:'行业应用',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/geli@2x.png',title:'猎熊座防疫隔离智能检测系统',text:'通过物联网、大数据手段，实现了“被隔离人员进出隔离场所”的实时预警功能。系统由前端监测采集器和后台数据分析处理系统组成。'},
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/zhatuche@2x.png',title:'渣土车城市运行综合管理中心智慧平台',text:'利用AI渣土车识别和RF-LOT智能感知技术，自动抓取渣土车辆高清图片，自动识别车辆车牌、车型等信息'},
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/kozhao@2x.png',title:'猎熊座口罩检测智能监测系统',text:'本产品主要应用于公共区域的人员口罩佩戴的智能检测，对实时捕捉到的画面分析，如有存在安全隐患的行为，实时进行语音提醒警报'},
  ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/shebei-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/shebei.png',name:'智能设备',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/GF010@2x.png',title:'智能设备',text:'猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。'},
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/shouhuan@2x.png',title:'老人手环',text:'老人智能手环运用公安物联网、RF-LOT智能感知等技术，可实时监测老人的行动轨迹，一旦发生老人走失，及时追踪寻回走失老人。'},
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/fangyi@2x.png',title:'防疫隔离智能监测系统',text:'采用4G无线监控球型摄像机用于疫情人员隔离观察及外出监管的防控系统，以便于基层防控人员对需要进行隔离观察的人群进行有效的管控。'},
  ]},
]
let selectedInner=[]
let productionInner = ()=>{
  let str = ``
  productionUrl.forEach((item,index)=>{
    // console.log(item.childern)
    str+=`
      <div class="pro-list-item ${item.selected?'pro-list-item-selected':''}" onclick='prolistitemClick(this,${index})'>
        <img src="${item.selected?item.sel:item.unsel}" alt="">
        </br>
        ${item.name}
      </div>
    `
    // return str;
  })
  $('.pro-list')[0].innerHTML = str

  let strPro = ``
  productionUrl[0].childern.forEach((item,index)=>{
    strPro+= "<div class='list-detail-con-item list-detail-con-item-" +index+1 + "' >"+
        "<div class='modelImg'>"+
          "<img src='" + item.image + "'></img>"+
        "</div>"+
        "<div class='modelTitle'>"
          + item.title+
        "</div>"+
        "<div class='modelText'>"
          +item.text+
        "</div>\n" +
        "        <div class='modelButton'>\n" +
        "          <button>了解更多</button>\n" +
        "        </div>\n" +
        "      </div>"
  })

  $('.list-detail-con')[0].innerHTML = strPro

  setTimeout(() => {
    $('.list-detail-con-item-1').animate({marginTop:"0px"},300)
    $('.list-detail-con-item-2').animate({marginTop:"0px"},400)
    $('.list-detail-con-item-3').animate({marginTop:"0px"},500)
  }, 100);

}
let anliUrl =  [
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renlian-1.png',selected:true, sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renlian.png',name:'人脸识别',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/GF010@2x.png',title:'人脸识别',text:'猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。'},
  ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chepai-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/chepai.png',name:'车牌识别',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/GF010@2x.png',title:'车牌识别',text:'猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。'},
  ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/diandongche-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/diandongche.png',name:'电动车防盗系统',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/GF010@2x.png',title:'电动车防盗系统',text:'猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。'},
  ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renqun-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/renqun.png',name:'人群密度分析',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/GF010@2x.png',title:'人群密度分析',text:'猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。'},
  ]},
  {unsel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/daozha-1.png',selected:false,sel:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/daozha.png',name:'小区道闸管理',childern:[
    {image:'https://bh-frontend.oss-cn-shanghai.aliyuncs.com/office-website/GF010@2x.png',title:'小区道闸管理',text:'猎熊座智能安全网关是面向人脸图像、车辆信息等接入层数据汇聚推出的基于公共网络安防数据结构化安全传输，具备运维管理的设备。'},
  ]},
]
let anliInner = ()=>{
  let str = ''
  anliUrl.forEach((item,index)=>{
    str+=`
      <div class="pro-list-item ${item.selected?'pro-list-item-selected':''}" onclick='anliListitemClick(this,${index})'>
        <img src="${item.selected?item.sel:item.unsel}" alt="">
        </br>
        ${item.name}
      </div>
    `
    // return str;
  })
  $('.pro-list')[0].innerHTML = str


  let strAnli = ''

  anliUrl[0].childern.forEach((item,index)=>{
    strAnli+=
      "<div class='list-detail-con-item list-detail-con-item-"+ index+1 +"'>"+
        "<div class='modelImg'>"+
          "<img src='"+item.image+"'></img>"+
        "</div>"+
        "<div class='modelTitle'>"
          +item.title+
        "</div>"+
        "<div class='modelText'>"
          +item.text+
        "</div>"+
        "<div class='modelButton'>"+
          "<button>了解更多</button>"+
        "</div>"+
      "</div>"
  })

  $('.list-detail-con')[0].innerHTML = strAnli

  setTimeout(() => {
    $('.list-detail-con-item-1').animate({marginTop:"0px"},300)
    $('.list-detail-con-item-2').animate({marginTop:"0px"},400)
    $('.list-detail-con-item-3').animate({marginTop:"0px"},500)
  }, 100);

}
productionInner()
$('.pro-title').click(function() {
  $('.pro-span').removeClass('pro-selected')
  $('.pro-span',this).addClass('pro-selected')
  if($('.pro-title',this)['prevObject'][0].innerText == '案例中心'){
    anliInner()

  }else{
    productionInner()

  }
})






let prolistitemClick = (data,index)=> {
  productionUrl.forEach((it,i)=>{
    it.selected = false
    if(i == index){
      it.selected = true
    }
  })
  productionInner()
  let str = ``

  productionUrl[index].childern.forEach((item,index)=>{
    str+=`
        <div class="list-detail-con-item list-detail-con-item-${index+1}" onclick=''>
          <div class='modelImg'>
            <img src='${item.image}'></img>
          </div>
          <div class='modelTitle'>
            ${item.title}
          </div>
          <div class='modelText'>
            ${item.text}
          </div>
          <div class='modelButton'>
            <button>了解更多</button>
          </div>
        </div>
      `
    })
  $('.list-detail-con')[0].innerHTML = str

  listJump()
}
let anliListitemClick = (data,index)=> {
  anliUrl.forEach((it,i)=>{
    it.selected = false
    if(i == index){
      it.selected = true
    }
  })
  anliInner()
  let str = ''
  // console.log(anliUrl[index].childern)
  anliUrl[index].childern.forEach((item,index)=>{
    str+=`
        <div class="list-detail-con-item list-detail-con-item-${index+1}" onclick=''>
          <div class='modelImg'>
            <img src='${item.image}'></img>
          </div>
          <div class='modelTitle'>
            ${item.title}
          </div>
          <div class='modelText'>
            ${item.text}
          </div>
          <div class='modelButton'>
            <button>了解更多</button>
          </div>
        </div>
      `
    })
  $('.list-detail-con')[0].innerHTML = str
  listJump()
}

let listJump = (data)=>{
  $('.list-detail-con-item-1').css({marginTop:'450px'})
  $('.list-detail-con-item-2').css({marginTop:'450px'})
  $('.list-detail-con-item-3').css({marginTop:'450px'})
  setTimeout(() => {
    $('.list-detail-con-item-1').animate({marginTop:"0px"},300)
    $('.list-detail-con-item-2').animate({marginTop:"0px"},400)
    $('.list-detail-con-item-3').animate({marginTop:"0px"},500)
  }, 100);

}
listJump()

$(document).on("mouseenter",".list-detail-con-item",function(){
  $(this).css({'box-shadow': '5px 5px 10px #ddd','transition': 'all .5s','margin-top':'-10px'})
})
$(document).on("mouseleave",".list-detail-con-item",function(){
  $(this).css({'box-shadow': 'none','transition': 'none','margin-top':'0px'})
})
